#!/bin/sh
# Translate the lynx_cfg.h and config.cache data into a table, useful for
# display at runtime.
#
# JMG 2021-08-21:
# This script has been taken from lynx2.9.0dev.6.tar.bz2 and is the last
# one known to work with the bash port of djgpp after djgpp specific adjustment.
# It will replace the one distributed with lynx that cannot be fixed anaymore
# until the bash port get fixed.
#

TOP="${1-.}"
OUT=cfg_defs.h

# just in case we want to run this outside the makefile
: ${SHELL:=/bin/sh}

cat >$OUT <<EOF
#ifndef CFG_DEFS_H
#define CFG_DEFS_H 1

static const struct {
	const char *name;
	const char *value;
} config_cache[] = {
EOF

sed \
	-e '/^#/d'     \
	-e 's/^.[^=]*_cv_//' \
	-e 's/=\${.*=/=/'  \
	-e 's/}$//'          \
	config.cache > ./pipe.tmp
	$SHELL $TOP/scripts/cfg_edit.sh < ./pipe.tmp >>$OUT
	rm -f ./pipe.tmp

cat >>$OUT <<EOF
};

static const struct {
	const char *name;
	const char *value;
} config_defines[] = {
EOF
fgrep	'#define' lynx_cfg.h |
sed	-e 's@	@ @g' \
	-e 's@  @ @g' \
	-e 's@^[ 	]*#define[ 	]*@@' \
	-e 's@[ ]*/\*.*\*/@@' \
	-e 's@[ 	][ 	]*@=@' \
    | $SHELL $TOP/scripts/cfg_edit.sh >>$OUT

cat >>$OUT <<EOF
};

#endif /* CFG_DEFS_H */
EOF
