@echo off
echo Configuring Lynx 2.9.0dev.10 for DJGPP v2.5...

Rem WARNING WARNING WARNING: This file needs to have DOS CRLF end-of-line
Rem format, or else stock DOS/Windows shells will refuse to run it.

Rem The SmallEnv tests protect against fixed and too small size
Rem of the environment in stock DOS shell.

Rem Find out if NLS is wanted or not,
Rem if dependency-tracking is wanted or not,
Rem if caching is wanted or not
Rem and where the sources are.
Rem We always default to NLS support,
Rem no dependency tracking
Rem and to in place configuration.
set ARGS=
set NLS=enabled
if not "%NLS%" == "enabled" goto SmallEnv
set CACHING=enabled
if not "%CACHING%" == "enabled" goto SmallEnv
set DEPENDENCY_TRACKING=disabled
if not "%DEPENDENCY_TRACKING%" == "disabled" goto SmallEnv
set LIBICONV_PREFIX=disabled
if not "%LIBICONV_PREFIX%" == "disabled" goto SmallEnv
set LIBINTL_PREFIX=disabled
if not "%LIBINTL_PREFIX%" == "disabled" goto SmallEnv
set HTML=enabled
if not "%HTML%" == "enabled" goto SmallEnv
set SILENT=enabled
if not "%SILENT%" == "enabled" goto SmallEnv
set ZLIB=enabled
if not "%ZLIB%" == "enabled" goto SmallEnv
set BZLIB=enabled
if not "%BZLIB%" == "enabled" goto SmallEnv
set SSL=enabled
if not "%SSL%" == "enabled" goto SmallEnv
set TRACE=enabled
if not "%TRACE%" == "enabled" goto SmallEnv
set VTRACE=enabled
if not "%VTRACE%" == "enabled" goto SmallEnv
set XSRC=.
if not "%XSRC%" == "." goto SmallEnv

Rem Loop over all arguments.
Rem Special arguments are: NLS, XSRC, CACHE, STATIC_LIBS, LIBICONV_PREFIX, LIBINTL_PREFIX and DEPS.
Rem All other arguments are stored into ARGS.
:ArgLoop
if "%1" == "nls" goto NextArgument
if "%1" == "NLS" goto NextArgument
if "%1" == "no-nls" goto NoNLS
if "%1" == "no-NLS" goto NoNLS
if "%1" == "NO-NLS" goto NoNLS
goto CachingOption
:NoNLS
if "%1" == "no-nls" set NLS=disabled
if "%1" == "no-NLS" set NLS=disabled
if "%1" == "NO-NLS" set NLS=disabled
if not "%NLS%" == "disabled" goto SmallEnv
goto NextArgument
:CachingOption
if "%1" == "cache" goto NextArgument
if "%1" == "CACHE" goto NextArgument
if "%1" == "no-cache" goto NoCaching
if "%1" == "no-CACHE" goto NoCaching
if "%1" == "NO-CACHE" goto NoCaching
goto DependencyOption
:NoCaching
if "%1" == "no-cache" set CACHING=disabled
if "%1" == "no-CACHE" set CACHING=disabled
if "%1" == "NO-CACHE" set CACHING=disabled
if not "%CACHING%" == "disabled" goto SmallEnv
goto NextArgument
:DependencyOption
if "%1" == "no-dep" goto NextArgument
if "%1" == "no-DEP" goto NextArgument
if "%1" == "NO-DEP" goto NextArgument
if "%1" == "dep" goto _DependencyTraking
if "%1" == "DEP" goto _DependencyTraking
goto LibiconvPrefixOption
:_DependencyTraking
if "%1" == "dep" set DEPENDENCY_TRACKING=enabled
if "%1" == "DEP" set DEPENDENCY_TRACKING=enabled
if not "%DEPENDENCY_TRACKING%" == "enabled" goto SmallEnv
goto NextArgument
:LibiconvPrefixOption
if "%1" == "no-libiconvprefix" goto NextArgument
if "%1" == "no-LIBICONVPREFIX" goto NextArgument
if "%1" == "NO-LIBICONVPREFIX" goto NextArgument
if "%1" == "libiconvprefix" goto WithLibiconvPrefix
if "%1" == "LIBICONVPREFIX" goto WithLibiconvPrefix
goto LibintlPrefixOption
:WithLibiconvPrefix
if "%1" == "libiconvprefix" set LIBICONV_PREFIX=enabled
if "%1" == "LIBICONVPREFIX" set LIBICONV_PREFIX=enabled
if not "%LIBICONV_PREFIX%" == "enabled" goto SmallEnv
goto NextArgument
:LibintlPrefixOption
if "%1" == "no-libintlprefix" goto NextArgument
if "%1" == "no-LIBINTLPREFIX" goto NextArgument
if "%1" == "NO-LIBINTLPREFIX" goto NextArgument
if "%1" == "libintlprefix" goto _WithLibintlPrefix
if "%1" == "LIBINTLPREFIX" goto _WithLibintlPrefix
goto HTMLOption
:_WithLibintlPrefix
if "%1" == "libintlprefix" set LIBINTL_PREFIX=enabled
if "%1" == "LIBINTLPREFIX" set LIBINTL_PREFIX=enabled
if not "%LIBINTL_PREFIX%" == "enabled" goto SmallEnv
:HTMLOption
if "%1" == "withhtml" goto NextArgument
if "%1" == "withHTML" goto NextArgument
if "%1" == "WITHHTML" goto NextArgument
if "%1" == "withouthtml" goto _WithoutHTML
if "%1" == "withoutHTML" goto _WithoutHTML
if "%1" == "WITHOUTHTML" goto _WithoutHTML
goto SilentOption
:_WithoutHTML
if "%1" == "withouthtml" set HTML=disabled
if "%1" == "withoutHTML" set HTML=disabled
if "%1" == "WITHOUTHTML" set HTML=disabled
if not "%HTML%" == "disabled" goto SmallEnv
goto NextArgument
:SilentOption
if "%1" == "silent" goto NextArgument
if "%1" == "SILENT" goto NextArgument
if "%1" == "no-silent" goto NoSILENT
if "%1" == "no-SILENT" goto NoSILENT
if "%1" == "NO-SILENT" goto NoSILENT
goto ZlibOption
:NoSILENT
if "%1" == "no-silent" set SILENT=disabled
if "%1" == "no-SILENT" set SILENT=disabled
if "%1" == "NO-SILENT" set SILENT=disabled
if not "%SILENT%" == "disabled" goto SmallEnv
goto NextArgument
:ZlibOption
if "%1" == "zlib" goto NextArgument
if "%1" == "ZLIB" goto NextArgument
if "%1" == "no-zlib" goto NoZLIB
if "%1" == "no-ZLIB" goto NoZLIB
if "%1" == "NO-ZLIB" goto NoZLIB
goto BzlibOption
:NoZLIB
if "%1" == "no-zlib" set ZLIB=disabled
if "%1" == "no-ZLIB" set ZLIB=disabled
if "%1" == "NO-ZLIB" set ZLIB=disabled
if not "%ZLIB%" == "disabled" goto SmallEnv
goto NextArgument
:BzlibOption
if "%1" == "bzlib" goto NextArgument
if "%1" == "BZLIB" goto NextArgument
if "%1" == "no-bzlib" goto NoBZLIB
if "%1" == "no-BZLIB" goto NoBZLIB
if "%1" == "NO-BZLIB" goto NoBZLIB
goto SslOption
:NoBZLIB
if "%1" == "no-bzlib" set BZLIB=disabled
if "%1" == "no-BZLIB" set BZLIB=disabled
if "%1" == "NO-BZLIB" set BZLIB=disabled
if not "%BZLIB%" == "disabled" goto SmallEnv
goto NextArgument
:SslOption
if "%1" == "ssl" goto NextArgument
if "%1" == "SSL" goto NextArgument
if "%1" == "no-ssl" goto NoSSL
if "%1" == "no-SSL" goto NoSSL
if "%1" == "NO-SSL" goto NoSSL
goto TraceOption
:NoSSL
if "%1" == "no-ssl" set SSL=disabled
if "%1" == "no-SSL" set SSL=disabled
if "%1" == "NO-SSL" set SSL=disabled
if not "%SSL%" == "disabled" goto SmallEnv
goto NextArgument
:TraceOption
if "%1" == "trace" goto NextArgument
if "%1" == "TRACE" goto NextArgument
if "%1" == "no-trace" goto NoTRACE
if "%1" == "no-TRACE" goto NoTRACE
if "%1" == "NO-TRACE" goto NoTRACE
goto VTraceOption
:NoTRACE
if "%1" == "no-trace" set TRACE=disabled
if "%1" == "no-TRACE" set TRACE=disabled
if "%1" == "NO-TRACE" set TRACE=disabled
if not "%TRACE%" == "disabled" goto SmallEnv
goto NextArgument
:VTraceOption
if "%1" == "vtrace" goto NextArgument
if "%1" == "VTRACE" goto NextArgument
if "%1" == "no-vtrace" goto NoVTRACE
if "%1" == "no-VTRACE" goto NoVTRACE
if "%1" == "NO-VTRACE" goto NoVTRACE
goto SrcDirOption
:NoVTRACE
if "%1" == "no-vtrace" set VTRACE=disabled
if "%1" == "no-VTRACE" set VTRACE=disabled
if "%1" == "NO-VTRACE" set VTRACE=disabled
if not "%VTRACE%" == "disabled" goto SmallEnv
goto NextArgument
:SrcDirOption
echo %1 | grep -q "/"
if errorlevel 1 goto CollectArgument
set XSRC=%1
if not "%XSRC%" == "%1" goto SmallEnv
goto NextArgument
:CollectArgument
set _ARGS=#%ARGS%#%1#
if not "%_ARGS%" == "#%ARGS%#%1#" goto SmallEnv
echo %_ARGS% | grep -q "###"
if errorlevel 1 set ARGS=%ARGS% %1
set _ARGS=
:NextArgument
shift
if not "%1" == "" goto ArgLoop

Rem Create an arguments file for the configure script.
echo --srcdir=%XSRC% > args
:if "%CACHING%" == "enabled"              echo --cache-file=%XSRC%/djgpp/config.cache >> args
if "%CACHING%" == "enabled"              echo --cache-file=./config.cache >> args
if "%DEPENDENCY_TRACKING%" == "enabled"  echo --enable-dependency-tracking >> args
if "%DEPENDENCY_TRACKING%" == "disabled" echo --disable-dependency-tracking >> args
if "%LIBICONV_PREFIX%" == "enabled"      echo --with-libiconv-prefix >> args
if "%LIBICONV_PREFIX%" == "disabled"     echo --without-libiconv-prefix >> args
if "%LIBINTL_PREFIX%" == "enabled"       echo --with-libintl-prefix >> args
if "%LIBINTL_PREFIX%" == "disabled"      echo --without-libintl-prefix >> args
if "%HTML%" == "enabled"                 echo --enable-html >> args
if "%HTML%" == "disabled"                echo --disable-html >> args
if "%SILENT%" == "enabled"               echo --enable-silent-rules >> args
if "%SILENT%" == "disabled"              echo --disable-silent-rules >> args
if "%ZLIB%" == "enabled"                 echo --with-zlib='/dev/env/DJDIR/lib' >> args
if "%ZLIB%" == "disabled"                echo --without-zlib >> args
if "%BZLIB%" == "enabled"                echo --with-bzlib='/dev/env/DJDIR/lib' >> args
if "%BZLIB%" == "disabled"               echo --without-bzlib >> args
if "%SSL%" == "enabled"                  echo --with-ssl='/dev/env/DJDIR/lib' >> args
if "%SSL%" == "disabled"                 echo --without-ssl >> args
if "%TRACE%" == "enabled"                echo --enable-trace >> args
if "%TRACE%" == "disabled"               echo --disable-trace >> args
if "%VTRACE%" == "enabled"               echo --enable-vertrace >> args
if "%VTRACE%" == "disabled"              echo --disable-vertrace >> args
if not "%ARGS%" == ""                    echo %ARGS% >> args

: These settings are from scripts/config.djgpp.sh
echo --prefix=/dev/env/DJDIR >> args
echo --disable-dired-override >> args
echo --disable-full-paths >> args
echo --enable-addrlist-page >> args
echo --enable-change-exec >> args
echo --enable-cgi-links >> args
echo --enable-charset-choice >> args
echo --enable-exec-links >> args
echo --enable-externs >> args
echo --enable-nested-tables >> args
echo --with-mime-libdir='/dev/env/DJDIR/etc' >> args

set ARGS=
set CACHING=
set DEPENDENCY_TRACKING=
set LIBICONV_PREFIX=
set LIBINTL_PREFIX=
set HTML=
set SILENT=
set ZLIB=
set BZLIB=
:set SSL= Variable wird nocht gebraucht und muss gesetzt bleiben.
set TRACE=
set VTRACE=


if "%XSRC%" == "." goto InPlace

:NotInPlace
redir -e /dev/null update %XSRC%/configure.org ./configure
test -f ./configure
if errorlevel 1 update %XSRC%/configure ./configure

:InPlace
Rem Update configuration files
echo Updating configuration scripts...
test -f ./configure.org
if errorlevel 1 update configure configure.org
sed -f %XSRC%/djgpp/config.sed configure.org > filename.ext
if errorlevel 1 goto Sed1Error
sed "/symlink /s/\x60//g" ./filename.ext > filename.out
mv -f ./filename.out ./configure
dtou ./configure
touch -r configure.org ./configure
rm -f ./filename.ext

Rem Make sure they have a config.site file
set CONFIG_SITE=%XSRC%/djgpp/config.site
if not "%CONFIG_SITE%" == "%XSRC%/djgpp/config.site" goto SmallEnv

Rem Make sure crucial file names are not munged by unpacking
test -f %XSRC%/config.h.in
if not errorlevel 1 redir -e /dev/null mv -f %XSRC%/config.h.in %XSRC%/config.h-in
test -f %XSRC%/config.h-in
if errorlevel 1 redir -e /dev/null mv -f %XSRC%/config.h %XSRC%/config.h-in
test -f %XSRC%/config.h-in
if errorlevel 1 redir -e /dev/null mv -f %XSRC%/configh.in %XSRC%/config.h-in
test -f %XSRC%/config.h-in
if errorlevel 1 redir -e /dev/null mv -f %XSRC%/config_h.in %XSRC%/config.h-in
test -f %XSRC%/lib/config.h.in
if not errorlevel 1 redir -e /dev/null mv -f %XSRC%/lib/config.h.in %XSRC%/lib/config.h-in
test -f %XSRC%/lib/config.h-in
if errorlevel 1 redir -e /dev/null mv -f %XSRC%/lib/config.h %XSRC%/lib/config.h-in
test -f %XSRC%/lib/config.h-in
if errorlevel 1 redir -e /dev/null mv -f %XSRC%/lib/configh.in %XSRC%/lib/config.h-in
test -f %XSRC%/lib/config.h-in
if errorlevel 1 redir -e /dev/null mv -f %XSRC%/lib/config_h.in %XSRC%/lib/config.h-in
test -f %XSRC%/po/Makefile.am.in
if not errorlevel 1 redir -e /dev/null mv -f %XSRC%/po/Makefile.am.in %XSRC%/po/Makefile.am-in
test -f %XSRC%/po/Makefile.am-in
if errorlevel 1 redir -e /dev/null mv -f %XSRC%/po/Makefile.am %XSRC%/po/Makefile.am-in
test -f %XSRC%/po/Makefile.in.in
if not errorlevel 1 redir -e /dev/null mv -f %XSRC%/po/Makefile.in.in %XSRC%/po/Makefile.in-in
test -f %XSRC%/po/Makefile.in-in
if errorlevel 1 redir -e /dev/null mv -f %XSRC%/po/Makefile.in %XSRC%/po/Makefile.in-in
test -f %XSRC%/build-aux/snippet/c++defs.h
if not errorlevel 1 redir -e /dev/null mv -f %XSRC%/build-aux/snippet/c++defs.h %XSRC%/build-aux/snippet/cxxdefs.h
:# Beginn  Zusaetzliche zu veraendernde Dateinamen.
:test -f %XSRC%/man/lynx.in
:if not errorlevel redir -e /dev/null 1 mv -f %XSRC%/man/lynx..in %XSRC%/man/lynx.-in
:test -f %XSRC%/man/lynx.-in
:if errorlevel 1 redir -e /dev/null mv -f %XSRC%/man/lynx. %XSRC%/man/lynx.-in
:# Ende  Zusaetzliche zu veraendernde Dateinamen.

Rem This is required because DOS/Windows are case-insensitive
Rem to file names, and "make install" will do nothing if Make
Rem finds a file called `install'.
if exist INSTALL ren INSTALL INSTALL.txt

Rem Set SHELL to a sane default or some configure tests stop working
Rem if the package is configured across partitions.
if not "%SHELL%" == "" goto HomeName
set SHELL=/bin/sh
if not "%SHELL%" == "/bin/sh" goto SmallEnv
echo No SHELL found in the environment, using default value

:HomeName
Rem Set HOME to a sane default so configure stops complaining.
if not "%HOME%" == "" goto HostName
set HOME=%XSRC%/djgpp
if not "%HOME%" == "%XSRC%/djgpp" goto SmallEnv
echo No HOME found in the environment, using default value

:HostName
Rem Set HOSTNAME so it shows in config.status
if not "%HOSTNAME%" == "" goto hostdone
if "%windir%" == "" goto msdos
set OS=MS-Windows
if not "%OS%" == "MS-Windows" goto SmallEnv
goto haveos
:msdos
set OS=MS-DOS
if not "%OS%" == "MS-DOS" goto SmallEnv
:haveos
if not "%USERNAME%" == "" goto haveuname
if not "%USER%" == "" goto haveuser
echo No USERNAME and no USER found in the environment, using default values
set HOSTNAME=Unknown PC
if not "%HOSTNAME%" == "Unknown PC" goto SmallEnv
goto userdone
:haveuser
set HOSTNAME=%USER%'s PC
if not "%HOSTNAME%" == "%USER%'s PC" goto SmallEnv
goto userdone
:haveuname
set HOSTNAME=%USERNAME%'s PC
if not "%HOSTNAME%" == "%USERNAME%'s PC" goto SmallEnv
:userdone
set _HOSTNAME=%HOSTNAME%, %OS%
if not "%_HOSTNAME%" == "%HOSTNAME%, %OS%" goto SmallEnv
set HOSTNAME=%_HOSTNAME%
:hostdone
set _HOSTNAME=
set OS=

Rem install-sh is required by the configure script but clashes with the
Rem various Makefile install-foo targets, so we MUST have it before the
Rem script runs and rename it afterwards
test -f %XSRC%/install-sh
if not errorlevel 1 goto NoRen0
test -f %XSRC%/install-sh.sh
if not errorlevel 1 mv -f %XSRC%/install-sh.sh %XSRC%/install-sh
:NoRen0

if "%NLS%" == "disabled" goto WithoutNLS

:WithNLS
test -d %XSRC%/po
if errorlevel 1 goto WithoutNLS

Rem Check for the needed libraries and binaries.
test -x /dev/env/DJDIR/bin/msgfmt.exe
if errorlevel 1 goto MissingNLSTools
test -x /dev/env/DJDIR/bin/xgettext.exe
if errorlevel 1 goto MissingNLSTools
test -f /dev/env/DJDIR/include/libcharset.h
if errorlevel 1 goto MissingNLSTools
test -f /dev/env/DJDIR/lib/libcharset.a
if errorlevel 1 goto MissingNLSTools
test -f /dev/env/DJDIR/include/iconv.h
if errorlevel 1 goto MissingNLSTools
test -f /dev/env/DJDIR/lib/libiconv.a
if errorlevel 1 goto MissingNLSTools
test -f /dev/env/DJDIR/include/libintl.h
if errorlevel 1 goto MissingNLSTools
test -f /dev/env/DJDIR/lib/libintl.a
if errorlevel 1 goto MissingNLSTools

Rem Recreate the files in the %XSRC%/po subdir with our ported tools.
redir -e /dev/null rm %XSRC%/po/*.gmo
redir -e /dev/null rm %XSRC%/po/lynx.pot
redir -e /dev/null rm %XSRC%/po/cat-id-tbl.c
redir -e /dev/null rm %XSRC%/po/stamp-cat-id

Rem Update the arguments file for the configure script.
Rem We prefer without-included-gettext because libintl.a from gettext package
Rem is the only one that is garanteed to have been ported to DJGPP.
echo --enable-nls --without-included-gettext >> args
goto ConfigurePackage

:MissingNLSTools
echo Needed DJGPP ports of the required libs/tools for NLS support not found.
echo Configuring without NLS.
:WithoutNLS
Rem Update the arguments file for the configure script.
echo --disable-nls >> args

:ConfigurePackage
echo Running the ./configure script...
sh ./configure @args
if errorlevel 1 goto RunConfigStatus
rm args
echo Done.
goto EditFiles
:RunConfigStatus
test -f ./config.status
if errorlevel 1 goto CfgError
echo Running the ./config.status script (manually)...
redir -e /dev/null sh ./config.status
if errorlevel 1 goto CfgError
rm args
echo Done.

:EditFiles
Rem replace cfg_defs.sh until bash port has been fixed.
echo Replace cfg_defs.sh...
cp -vf %XSRC%/djgpp/cfg_defs.sh %XSRC%/scripts/cfg_defs.sh
if "%SSL%" == "disabled" goto end
echo Editing lynx_cfg.h...
update ./lynx_cfg.h ./lynx_cfg_h.org
sed -f %XSRC%/djgpp/config_h.sed ./lynx_cfg_h.org > lynx_cfg.h
if errorlevel 1 goto Sed2Error
echo Done.

:NextEdit
echo Editing src/Makefile...
:test -f ./src/Makefile.org
:if errorlevel 1 update ./src/Makefile ./src/Makefile.org
update ./src/Makefile ./src/Makefile.org
sed -f %XSRC%/djgpp/Makefile.sed ./src/Makefile.org > Makefile.src
if errorlevel 1 goto Sed3Error
mv Makefile.src ./src/Makefile
echo Done.

echo Editing src/chrtrans/Makefile...
:test -f ./src/chrtrans/Makefile.org
:if errorlevel 1 update ./src/chrtrans/Makefile ./src/chrtrans/Makefile.org
update ./src/chrtrans/Makefile ./src/chrtrans/Makefile.org
sed -f %XSRC%/djgpp/Makefile.sed ./src/chrtrans/Makefile.org > Makefile.chr
if errorlevel 1 goto Sed4Error
mv Makefile.chr ./src/chrtrans/Makefile
echo Done.

echo Editing Makefile...
:test -f ./Makefile.org
:if errorlevel 1 update ./Makefile ./Makefile.org
update ./Makefile ./Makefile.org
sed -f %XSRC%/djgpp/Makefile.sed ./Makefile.org > Makefile.txt
if errorlevel 1 goto Sed5Error
mv Makefile.txt ./Makefile
echo Done.

Rem Create po/LINGUAS file or the build fails.
::update %XSRC%/po/LINGUAS ./po/LINGUAS
goto End

:Sed1Error
echo ./configure script editing failed!
goto End

:Sed2Error
echo lynx_cfg.h fixing failed!
goto End

:Sed3Error
echo src/Makefile fixing failed!
goto End

:Sed4Error
echo src/chrtrans/Makefile fixing failed!
goto End

:Sed5Error
echo Makefile fixing failed!
goto End

:Sed6Error
echo doc/Makefile fixing failed!
goto End

:Sed7Error
echo tests/Makefile fixing failed!
goto End

:Sed8Error
echo ./ltmain.sh editing failed!
goto End

:CfgError
echo ./configure script exited abnormally!
goto End

:SmallEnv
echo Your environment size is too small.  Enlarge it and run me again.
echo Configuration NOT done!

:End
test -f %XSRC%/install-sh.sh
if not errorlevel 1 goto NoRen1
test -f %XSRC%/install-sh
if not errorlevel 1 mv -f %XSRC%/install-sh %XSRC%/install-sh.sh
:NoRen1
if "%SHELL%" == "/bin/sh" set SHELL=
if "%HOME%" == "%XSRC%/djgpp" set HOME=
set ARGS=
set CONFIG_SITE=
set HOSTNAME=
set NLS=
set CACHING=
set DEPENDENCY_TRACKING=
set SILENT=
set ZLIB=
set BZLIB=
set SSL=
set TRACE=
set VTRACE=
set XSRC=
