# Sed script to add DJGPP specific issues to config.h.

# Use openssl.
## /USE_OPENSSL_INCL/ {
## s|/\*[ 	]*#undef|#define|
## s|\*/|1|
## }
## /USE_SSL/ {
## s|/\*[ 	]*#undef|#define|
## s|\*/|1|
## }

$ a\
\
\
/* DGJPP-specific definitions */\
\
#ifdef __DJGPP__\
/*  gcc no longer includes this by default.  */\
# include <sys/version.h>\
#endif
