#  Sed script to adapt the diffrent Makefiles.
/^PATH_SEPARATOR[ 	]*=/ s/:$/;/

# Piping to called make seems not to work with DJGPP's ports of bash.
/^install-help[ 	]*:/,/^$/ {
  /echo Updating .*lynx.cfg/,/-rm/ {
    s,>,lynx1.txt > ,
    /SYSCONFDIR)\/lynx.tmp/ s,lynx.tmp |,lynx.tmp > lynx1.txt ;,
    /lynx_help/ s,\$(helpdir) |,'/dev/env/DJDIR/share/lynx_help' lynx1.txt > lynx2.txt ;,
    /lynx_doc/ s,\$(helpdir) |,'/dev/env/DJDIR/share/lynx_help' lynx2.txt > lynx1.txt ;,
  }
  /-rm / s/$/ lynx1.txt lynx2.txt/
}

# Remove install-binSCRIPTS from dependecy list of install-exec-am in ./src/Makefile
/^install-exec-am[ 	]*:/ s/install-binSCRIPTS/#&/

# For some reason the configure script does neither recognize that
# OppenSSL is installed and functional and that it requires to
# be linked with zlib nor that libidn requires to be linked
# with libiconv.
/^LIBS[ 	]*=/ {
  s/-lwatt/-lssl -lcrypto -lwatt -lz/
  s/-lidn/-lidn -liconv/
}

# Change path separator.
/^VPATH[ 	]*=/ s/:/;/
